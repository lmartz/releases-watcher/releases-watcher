# Releases watcher

Releases watcher is a python tool that allows you to subscribe to git repositories and will alert you in slack when a new release is out for a subscribed repository, allowing you to keep an eye on the tools you use.
For the time being, you can only subscribe to github and be alerted through slack, but more platforms are coming soon.

This tool tracks releases evolution with a json based database.
You have to provide a yaml config file describing the repositories you want to track.

![image info](./pictures/slack-watcher.png)

## Installation & usage

You can install the releases-watcher with pip, docker or helm (recommended).
Whether you choose pip, docker or helm, some features are common and work the same.

### Alerting

you have the choice to enable or disable the alerting.
When disabled, you have to check the logs of each run to know if a release you are tracking has been upgraded, it is suboptimal.
When alerting is enabled you have to configure a bot in you slack tenant in order to be alerted.
It is really fast to do and you can have more info [here](https://slack.com/help/articles/115005265703-Create-a-bot-for-your-workspace)

### Subscriptions yaml file

The core of this tool is based of a yaml file that you'll provide to the software so it know what are the repositories you want to track.
The yaml file must be a list of repositories such as :

```yaml
subscriptions:
  - repo_name: kubernetes
    repo_owner: kubernetes
  - repo_name: terraform
    repo_owner: hashicorp
  - repo_name: terragrunt
    repo_owner: gruntwork-io
  - repo_name: grafana
    repo_owner: grafana
  - repo_name: pre-commit-hooks
    repo_owner: pre-commit
  - repo_name: longhorn
    repo_owner: longhorn
  - repo_name: awx-operator
    repo_owner: ansible
  - repo_name: kaniko
    repo_owner: GoogleContainerTools
```

### Environment variables

if you dont want to provide the subscription config file via the CLI, you can set it via en environment variable:

```bash
YAML_CONFIG_FILE=<path to the file>
```

You also have to provide a path for the tracking db file, its normal that you don't have one on the first run and it will be created.
you can provide it via the CLI or via an env var :

```bash
TRACKING_DB_FILE=<path where the db will be created>
```

it is handy that you can provide the db file as such as it allow a way for you to import a db from a previous usage without losing the tracking state.

If you enabled the alerting, slack needs two variables in order to work:

```bash
SLACK_CHANNEL_ID
SLACK_BOT_TOKEN
```

### Database data

On the first run, you will build the baseline of the database, and an output similar to this one will be displayed :

```code
No record exists in the DB for the kubernetes/kubernetes subscription.
Building one now, you'll be ready for the next releases
```

So as stated here, this will only build a base, and you'll be ready for the next runs if persistence is enabled.

then, as you add more subscriptions, you'll alway have to run it once before having useful data to us.

## Installation with Helm

Use [Helm](https://helm.sh/) to install releases-watcher.
The helm chart repo can be found [here](https://gitlab.com/lmartz/releases-watcher/releases-watcher-helm-chart)

```bash
helm repo add releases-watcher https://gitlab.com/api/v4/projects/37322844/packages/helm/stable
helm install <release-name> releases-watcher/releases-watcher
```

#### Installation with a specific values file :

```bash
helm install <release-name> releases-watcher/releases-watcher -f <path to values file>
```

### Usage with Helm

By default no data persistence is enabled so its ideal if you want to try it, but for the long run the idea is to run it on a schedule(kubernetes cronjob) and keep track of releaes movements between each run.

The yaml config and tracking db files are running in /app/config

To enable persistence :

```yaml
persistence:
  enable: true
  volume:
    name: pvc-persistence
    size: 2Gi
    storageClassName: <you storage class>
```

To enable / disable alerting :

```yaml
alerts:
  enabled: false / true
  alert_platform: slack #-> this must be uncommented if true, commented if false
```

Instead of using the basic provided config , you would have to provide you own set of config via a values.yaml file.
To grasp a good idea on what values you can change, please check the [values.yaml](https://gitlab.com/lmartz/releases-watcher/releases-watcher-helm-chart/-/blob/main/values.yaml)

NOTE: When installing with helm, the subscription config file is provided via a config map, so you don't have so provide a separate file by yourself, instead you must pass your subscriptions through the values file such as :

```yaml
subscriptions:
  - repo_name: kubernetes
    repo_owner: kubernetes
  - repo_name: terraform
    repo_owner: hashicorp
```

To manage the schedule:
if you aren't familiar with cron schedules, please see the [Crontab-Guru](https://crontab.guru/)

```yaml
schedule: "0 8,12,15 * * *"
```

Kubernetes 1.24 introduced the [timezones controller feature-gate](https://kubernetes.io/docs/concepts/workloads/controllers/cron-jobs/#time-zones) allowing you to choose a time zone for your schedule.
If your controller is configured to use this feature gate , you can specify it to the helm chart :

```yaml
featureGates:
  timeZone:
    enabled: true
    name: America/Montreal
```

if you don't have it enabled, leave this config to false.

Environment variables:
If needed , the chart is built to allow you to inset as many env var or env var from secrets as you want.
So for exemple let say you want to use the slack alerting, then you would have to pass two env var to kubernetes.
You could chose to pass them directly via simple env var as such :

```yaml
envVars:
  - name: SLACK_CHANNEL_ID
    value: my_slack_id
  - name: SLACK_BOT_TOKEN
    value: my_slack_bot_token
```

of course it is not advised to use it like that as the value would be displayed.
So you can use the env from secret as such :

```yaml
envFromSecret:
  - envName: SLACK_CHANNEL_ID
    secretName: slack-creds
    key: SLACK_CHANNEL_ID
  - envName: SLACK_BOT_TOKEN
    secretName: slack-creds
    key: SLACK_BOT_TOKEN
```

where:

- "slack-creds" is the name of a secret existing in the same namespace as the one you are planning to deploy to.
- the key is the key to reference whitin the secret
- the envName is the name of the env var that will exists within the container.

## Installation with pip

Installing it with pip will allow you to run it on your localhost with the CLI.

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install releases-watcher.

```bash
pip install releases-watcher --extra-index-url https://gitlab.com/api/v4/projects/37319786/packages/pypi/simple
```

if you need help with the script, just run the help command :

```bash
releases-watcher --help
```

For a basic usage you would have to call it with:

- the subscription file (it must exist on your system)
- the path of the db file if you already have one, else it will be created at specified path
- whether you want to enable the alerting or not
- if alerting is enabled, you have to specify the alerting plarform (only slack is supported for the time being)

so it would look like this :

```bash
releases-watcher subscriptions.yaml tracking-db.json --alert --alert-platform=slack
```

if you want it to run on a schedule, you would have to run it via a cronJob.

## Installation with Docker

Use [docker](https://www.docker.com/) to install releases-watcher.
Please check the [releases tags](https://gitlab.com/lmartz/releases-watcher/releases-watcher/container_registry/3180039) in order to pull the most recent one.

```bash
docker pull registry.gitlab.com/lmartz/releases-watcher/releases-watcher:<tag>
```

By default no data persistence is enabled, so for the long run the idea is to run it with a volume to keep the data generated by the tracking db.

with docker you can make use of [volumes](https://docs.docker.com/storage/volumes/) to persist data.

You would have to run the container with a volume, and mount within it the subscription yaml file :

```bash
docker container run -d \
    -v "$(pwd)"/subscriptions.yml:/app/subscriptions.yml:ro \ #mounting your subscriptions file
    -v db-data:/app/config \ # creating a volume that represent the db data location.
    registry.gitlab.com/lmartz/releases-watcher/releases-watcher:0.0.1 \
    /app/subscriptions.yml \ # this provides the path of the config file from whithin the container
    /app/config/data.json \ # this provides the path where the db will be created and persisted
    --alert \ # allowing or disabling the alerting
    --alert-platform=slack # only use if alerting is enabled
```

Environment variables:
you can pass env vars to the container (for slack for instance) via the -e flag.
I would advise against passing the value directly to the docker command, but instead, exporting the value as en env var on the system and referencing it such as :

```bash
export SLACK_CHANNEL_ID="my_channel_id"
export SLACK_BOT_TOKEN="my_slack_bot_token"


docker container run -d \
    -e SLACK_CHANNEL_ID=$SLACK_CHANNEL_ID \
    -e SLACK_BOT_TOKEN=$SLACK_BOT_TOKEN \
    -v "$(pwd)"/subscriptions.yml:/app/subscriptions.yml:ro \ #mounting your subscriptions file
    -v db-data:/app/config \ # creating a volume that represent the db data location.
    registry.gitlab.com/lmartz/releases-watcher/releases-watcher:0.0.1 \
    /app/subscriptions.yml \ # this provides the path of the config file from whithin the container
    /app/config/data.json \ # this provides the path where the db will be created and persisted
    --alert \ # allowing or disabling the alerting
    --alert-platform=slack # only use if alerting is enabled
```

In order to run it on a schedule, you must use a cronjob.

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License

[MIT](https://choosealicense.com/licenses/mit/)
