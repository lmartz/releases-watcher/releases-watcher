ARG alpine_version=3.15.0

#Setting up a light base image
FROM alpine:${alpine_version} as base
ENV PYTHON_VERSION="3.9.7-r4"
SHELL ["/bin/ash", "-o", "pipefail", "-c"]
RUN apk update && apk upgrade && apk add --no-cache \
    py3-pip="$(apk info py3-pip | head -n1 | cut -d' ' -f1 | cut -d'-' -f3-)" \
    python3=${PYTHON_VERSION}

#Build Stage
FROM base as builder
ENV POETRY_VERSION="1.1.13" \
    POETRY_HOME="/opt/poetry" \
    POETRY_NO_INTERACTION="1" \
    PIP_NO_CACHE_DIR="off" \
    PIP_DISABLE_PIP_VERSION_CHECK="on"
SHELL ["/bin/ash", "-o", "pipefail", "-c"]
RUN apk add --no-cache \
    curl="$(apk info curl | head -n1 | cut -d' ' -f1 | cut -d'-' -f2-)" \
    && curl -Lo /tmp/get-poetry.py https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py \
    && python3 /tmp/get-poetry.py \
    && rm -f /tmp/get-poetry.py
WORKDIR /src
COPY . .
RUN $POETRY_HOME/bin/poetry build

#Installing from build stage, running from base image
FROM base as runtime
ARG VERSION
ENV PIP_NO_CACHE_DIR="off" \
    PIP_DISABLE_PIP_VERSION_CHECK="on"
COPY --from=builder /src/dist /src
WORKDIR /app
RUN pip install --no-cache-dir /src/releases_watcher-${VERSION}-py3-none-any.whl \
    && rm -rf /src \
    && addgroup -Sg 10100 releases-watcher \
    && adduser -Su 10100 -G releases-watcher releases-watcher \
    && mkdir /app/config \
    && chown releases-watcher: -R /app \
    && chmod 700 -R /app
USER releases-watcher
ENTRYPOINT [ "releases-watcher" ]
