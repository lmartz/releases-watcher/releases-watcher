from pathlib import Path

import pytest
import tinydb.queries
import tinydb.table
from schema import SchemaError
from schema import SchemaMissingKeyError
from tinydb import where

from releases_watcher import __version__
from releases_watcher.main import build_data
from releases_watcher.main import check_alert_platform
from releases_watcher.main import check_alerts_args
from releases_watcher.main import check_file_extension
from releases_watcher.main import check_for_rate_limiting
from releases_watcher.main import check_if_yaml_exists
from releases_watcher.main import initialize_tracking_db
from releases_watcher.main import insert_data
from releases_watcher.main import open_query
from releases_watcher.main import record_query
from releases_watcher.main import send_notification
from releases_watcher.main import tag_comparison
from releases_watcher.main import validate_conf_file
from releases_watcher.slack_alerting import slack_alert

# from schema import Schema


@pytest.fixture
def valid_yaml_file():
    return (
        Path(__file__).resolve().parent / "mockups_files" / "subscriptions.yml"
    )


@pytest.fixture
def valid_db_file():
    return Path(__file__).resolve().parent / "mockups_files" / "tracking.json"


def test_version():
    assert __version__ == "0.0.2"


def test_check_file_extension(capsys):
    yaml_allowed_ext = [".yml", ".yaml"]
    bad_file_path = Path("file.yoml")
    # Test exit code when file does not exit
    with pytest.raises(SystemExit) as e:
        check_file_extension(bad_file_path, "yaml")
    captured = capsys.readouterr()
    assert e.type == SystemExit
    assert e.value.code == 1
    assert (
        captured.err
        == f"the file's extension should be: {yaml_allowed_ext} | Got: {bad_file_path.suffix}\n"
    )


def test_validate_conf_file(capsys, valid_yaml_file):
    invalid_conf_file_key = (
        Path(__file__).resolve().parent
        / "mockups_files"
        / "subscriptions_inv.yml"
    )
    invalid_conf_file_type = (
        Path(__file__).resolve().parent
        / "mockups_files"
        / "subscriptions_inv2.yml"
    )

    # Returns SchemaMissingKeyError if invalid or missing  subscription key
    with pytest.raises(SchemaError) as e:
        validate_conf_file(invalid_conf_file_key)
    assert e.type == SchemaMissingKeyError
    captured = capsys.readouterr()
    # Check if the string "Invalid yaml" is in the returned output on invalid file
    assert "Invalid YAML:" in captured.err

    # Returns SchemaError if invalid schema within the list
    with pytest.raises(SchemaError) as e:
        validate_conf_file(invalid_conf_file_type)
    assert e.type == SchemaError

    # Returns a dict of loaded yaml if valid schema
    assert isinstance(validate_conf_file(valid_yaml_file), dict)


def test_check_if_yaml_exists(capsys, valid_yaml_file):
    non_existing_path = Path("#")

    # Test exit code when file does not exit
    with pytest.raises(SystemExit) as e:
        check_if_yaml_exists(non_existing_path)
    captured = capsys.readouterr()
    assert (
        captured.err
        == f'the file: "{non_existing_path}" does not exist, please verify the path and try again\n'
    )
    assert e.type == SystemExit
    assert e.value.code == 1

    # Test the the returned value is True and that the triggering value is actually resolved as Path
    exists = check_if_yaml_exists(valid_yaml_file)
    assert isinstance(exists, Path)


def test_initialize_tracking_db(capsys, valid_db_file):
    accepted_ext = [".json"]
    non_existing_db = (
        Path(__file__).resolve().parent / "mockups_files" / "track.json"
    )
    non_existing_db_bad_ext = (
        Path(__file__).resolve().parent / "mockups_files" / "track.db"
    )
    initialize_tracking_db(valid_db_file)
    assert valid_db_file.is_file()

    initialize_tracking_db(non_existing_db)
    assert non_existing_db.is_file()
    assert non_existing_db.suffix == accepted_ext[0]
    non_existing_db.unlink()

    with pytest.raises(SystemExit) as e:
        initialize_tracking_db(non_existing_db_bad_ext)
    captured = capsys.readouterr()
    assert (
        captured.err
        == f"the file's extension should be: {accepted_ext} | Got: {non_existing_db_bad_ext.suffix}\n"
    )
    assert e.type == SystemExit
    assert e.value.code == 1


def test_check_alert_platform():
    ret = False
    with pytest.raises(SystemExit) as e:
        ret = check_alert_platform("msn")
    assert e.type == SystemExit
    assert e.value.code == 1
    assert ret is not True
    ret = check_alert_platform("slack")
    assert ret


def test_api_call():
    pass


def test_check_for_rate_limiting():
    mockup_rate_limited_data = {"message": "rate_limited"}
    mockup_data = {
        "tag_name": "v1.24.1",
        "html_url": "https://mockup.com",
        "published_at": "2022-05-26T03:31:07Z",
        "body": "mockup body",
    }
    with pytest.raises(SystemExit) as e:
        check_for_rate_limiting(mockup_rate_limited_data)
    assert e.type == SystemExit
    assert e.value.code == 1

    assert check_for_rate_limiting(mockup_data)


def test_build_data():
    mockup_response_data = {
        "tag_name": "v1.24.1",
        "html_url": "https://mockup.com",
        "published_at": "2022-05-26T03:31:07Z",
        "body": "mockup body",
    }
    data = build_data(mockup_response_data)
    for k in mockup_response_data.keys():
        assert data.get(k, None) == mockup_response_data.get(k, None)
    assert isinstance(data, dict)


def test_record_query(valid_db_file):
    table = initialize_tracking_db(valid_db_file)
    r_owner = r_name = "prometheus"
    search_result = record_query(table, r_owner, r_name)
    assert type(search_result) == list
    assert bool(search_result) is not False


def test_open_query():
    q = open_query()
    assert isinstance(q, tinydb.queries.Query)


def test_tag_comparison(valid_db_file, capsys):
    mockup_response_data_lower_tag = {
        "tag_name": "v1.24.1",
        "html_url": "https://mockup.com",
        "published_at": "2022-05-26T03:31:07Z",
        "body": "mockup body",
    }
    mockup_response_data_higher_tag = {
        "tag_name": "v3.24.1",
        "html_url": "https://mockup.com",
        "published_at": "2022-05-26T03:31:07Z",
        "body": "mockup body",
    }
    table = initialize_tracking_db(valid_db_file)
    r_owner = r_name = "prometheus"

    ret = tag_comparison(
        table, mockup_response_data_lower_tag, r_owner, r_name
    )

    captured = capsys.readouterr()
    assert "No new tags for today" in captured.out
    assert ret is None

    ret = tag_comparison(
        table, mockup_response_data_higher_tag, r_owner, r_name
    )

    captured = capsys.readouterr()
    assert "No new tags for today" not in captured.out
    assert ret
    # # Reset the tag that has been updated to its default value allowing tests to keep running next time
    releases = tinydb.queries.Query()
    table.update(
        {
            "current_version": mockup_response_data_lower_tag.get(
                "tag_name", None
            )
        },
        releases.repo_owner == r_owner,
    )


def test_send_notification(capsys):
    mockup_data = {
        "tag_name": "v3.24.1",
        "html_url": "https://mockup.com",
        "published_at": "2022-05-26T03:31:07Z",
        "body": "mockup body",
    }
    r_owner = r_name = "prometheus"
    send_notification("slack", mockup_data, r_owner, r_name, py_test=True)
    captured = capsys.readouterr()
    assert captured.out == "notification sent to Slack\n"


def test_insert_data(valid_db_file, capsys):
    mockup_data = {
        "tag_name": "v1.24.1",
        "html_url": "https://mockup.com",
        "published_at": "2022-05-26T03:31:07Z",
        "body": "mockup body",
    }
    table = initialize_tracking_db(valid_db_file)
    r_owner = r_name = "mockup_repo"

    ret = insert_data(table, mockup_data, r_owner, r_name)

    assert ret is None
    releases = open_query()
    searched_insert = table.search(
        (releases.repo_owner == r_owner) & (releases.repo_name == r_name)
    )
    assert (
        r_owner in searched_insert[0].values()
        and r_name in searched_insert[0].values()
    )
    # reset to mockup db to keep the next test running fine
    table.remove(where("repo_owner") == r_owner)
    table = initialize_tracking_db(
        Path(__file__).resolve().parent / "mockups_files" / "emptydb.json"
    )
    ret = insert_data(table, mockup_data, r_owner, r_name)
    captured = capsys.readouterr()
    assert ret is None
    assert (
        f"No record exists in the DB for the {r_owner}/{r_name} subscription.\n"
        in captured.out
    )
    assert (
        "Building one now, you'll be ready for the next releases\n"
        in captured.out
    )
    releases = open_query()
    searched_insert = table.search(
        (releases.repo_owner == r_owner) & (releases.repo_name == r_name)
    )
    assert (
        r_owner in searched_insert[0].values()
        and r_name in searched_insert[0].values()
    )
    table.remove(where("repo_owner") == r_owner)


def test_slack_alert():
    mockup_data = {
        "tag_name": "v1.24.1",
        "html_url": "https://mockup.com",
        "published_at": "2022-05-26T03:31:07Z",
        "body": "mockup body",
    }
    r_owner = r_name = "mockup_repo"
    ret = slack_alert(mockup_data, r_owner, r_name, py_test=True)
    assert type(ret) == str
    assert r_owner in ret


def test_check_alerts_args(capsys):
    with pytest.raises(SystemExit) as e:
        check_alerts_args(True, None)
    assert e.type == SystemExit
    assert e.value.code == 1
    captured = capsys.readouterr()
    assert (
        captured.err
        == "If the alert option is set to true, you have to pass an alert platform with --alert-platform\n"
    )
    with pytest.raises(SystemExit) as e:
        check_alerts_args(False, "slack")
    assert e.type == SystemExit
    assert e.value.code == 1
    captured = capsys.readouterr()
    assert (
        captured.err
        == "If the alert option is set to false, you can't use the --alert-platform option.\n"
    )
