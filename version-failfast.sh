#!/bin/bash

CI_DECLARED_VERSION=${1}
PYPROJECT_VERSION=${2}
PY_INIT_VERSION=${3}
PYTEST_VERSION=${4}
LAST_UPLOADED_VERSION=${5}
FILES_WITH_VERSION=("pyproject.toml | ${PYPROJECT_VERSION}" "releases_watcher/__init__.py | ${PY_INIT_VERSION}" "tests/test_releases_watcher.py | ${PYTEST_VERSION}" )


function version_compare_files {
    if [ "${CI_DECLARED_VERSION}" == "${2}" ]
    then
        echo "The version declared within the CI file matches the one declared in ${1} - Moving on "
    else
        echo "ERROR: The version declared within the CI file ( ${CI_DECLARED_VERSION} ) does not match the one declared in ${1}"
        exit 1
    fi
}

for i in "${FILES_WITH_VERSION[@]}";
do
    FILE=$( echo "${i}" | cut -d"|" -f1 | xargs)
    VERSION=$( echo "${i}" | cut -d"|" -f2 | xargs)
    version_compare_files "${FILE}" "${VERSION}"
done
printf "all the files have matching versions - Continuing.\n"
echo "Now checking that the package version defined in the CI is different from the last uploaded one"

if [ "${LAST_UPLOADED_VERSION}" == "null" ]
then
    echo "No packages are in the package registry, nothing to compare, moving on..."
    exit 0
elif [ "${CI_DECLARED_VERSION}" == "${LAST_UPLOADED_VERSION}" ]
then
    echo "ERROR: The package version you are trying to push ( ${CI_DECLARED_VERSION} )  is the same as the last uploaded version ( ${LAST_UPLOADED_VERSION} )."
    exit 1
fi
echo "Everything is okay - Moving on"
