import os
from typing import Union

from slack_sdk import WebClient
from slack_sdk.errors import SlackApiError


def slack_alert(
    data: dict, r_owner: str, r_name: str, py_test: bool = False
) -> Union[WebClient, str, None]:
    slack_token = os.environ["SLACK_BOT_TOKEN"]
    slack_channel_id = os.environ["SLACK_CHANNEL_ID"]
    client = WebClient(token=slack_token)
    mrkdwn_message_template = f"Hey <!channel>\n:rocket: NEW RELEASE ALERT ! :rocket:\n```{r_owner} released {r_name} {data.get('tag_name',None)}``` \n YOU CAN GRAB MORE INFO AT \n```{data.get('html_url',None)}``` \nHERE IS WHAT CHANGED \n```{data.get('body',None)}``` \nEnjoy the rest of your day !"
    pure_text_message_template = f"""
Hey <!channel>
:rocket: NEW RELEASE ALERT ! :rocket:
```{r_owner} released {r_name} {data.get('tag_name',None)}```
YOU CAN GRAB MORE INFO AT
```{data.get('html_url',None)}```
HERE IS WHAT CHANGED
```{data.get('body',None)}```
Enjoy the rest of your day !"""
    try:
        return (
            client.chat_postMessage(
                channel=slack_channel_id,
                text=pure_text_message_template,
                blocks=[
                    {
                        "type": "section",
                        "text": {
                            "type": "mrkdwn",
                            "text": mrkdwn_message_template,
                        },
                    }
                ],
            )
            if not py_test
            else mrkdwn_message_template
        )
    except SlackApiError as e:
        print(f"Error: {e}")
    return None
