import sys
from datetime import date
from pathlib import Path
from typing import Union

import requests
import tinydb.queries
import tinydb.table
import typer
import yaml
from schema import Schema
from schema import SchemaError
from tinydb import TinyDB

from releases_watcher.slack_alerting import slack_alert


def check_alerts_args(alert: bool, alert_platform: str):
    if alert and alert_platform is None:
        typer.echo(
            "If the alert option is set to true, you have to pass an alert platform with --alert-platform",
            err=True,
        )
        sys.exit(1)
    elif alert_platform is not None and not alert:
        typer.echo(
            "If the alert option is set to false, you can't use the --alert-platform option.",
            err=True,
        )
        sys.exit(1)


def check_if_yaml_exists(path: str) -> Path:
    converted_path = Path(path)
    if not converted_path.is_file():
        typer.echo(
            f'the file: "{converted_path}" does not exist, please verify the path and try again',
            err=True,
        )
        sys.exit(1)
    return converted_path


def check_file_extension(file: Path, file_kind: str) -> bool:
    expected_ext = {"yaml": [".yml", ".yaml"], "db": [".json"]}
    if file.suffix not in expected_ext[file_kind]:
        typer.echo(
            f"the file's extension should be: {expected_ext[file_kind]} | Got: {file.suffix}",
            err=True,
        )
        sys.exit(1)
    return True


def validate_conf_file(file: Path) -> dict:
    config_schema = Schema(
        {"subscriptions": [{"repo_name": str, "repo_owner": str}]}
    )

    with open(file) as f:
        conf_yaml = yaml.safe_load(f)
    try:
        config_schema.validate(conf_yaml)

    except SchemaError as se:
        typer.echo(f"Invalid YAML: {se}", err=True)
        raise se

    return conf_yaml


def initialize_tracking_db(path: Path) -> TinyDB:
    check_file_extension(path, "db")
    return TinyDB(path, indent=4).table("releases")


def check_alert_platform(alert_platform: str) -> bool:
    VALID_ALERT_PLATFORMS = ["slack"]
    if alert_platform and alert_platform.lower() not in VALID_ALERT_PLATFORMS:
        typer.echo(
            f"For the time being, only {VALID_ALERT_PLATFORMS} are valid alerting platforms",
            err=True,
        )
        typer.echo(
            'Either you disable alerting with "--no-alert" flag and just use the CLI output for checking the releases updates or you use one of the valid alerting platforms',
            err=True,
        )
        sys.exit(1)
    return True


def api_call(r_owner: str, r_name: str) -> dict:
    # TODO: when I open more api usage such as gitlab, a dict would be nice to map a company with its default api url.
    constructed_url = (
        f"https://api.github.com/repos/{r_owner}/{r_name}/releases/latest"
    )

    resp = requests.get(constructed_url).json()
    check_for_rate_limiting(resp)
    return build_data(resp)


def build_data(json_response: dict) -> dict:
    data = {}
    fetched_keys = ["tag_name", "html_url", "published_at", "body"]
    try:
        for key in fetched_keys:
            data[key] = json_response.get(key, None)
    except Exception as e:
        print(f"Error: {e}")
    return data


def check_for_rate_limiting(json_data: dict) -> bool:
    try:
        if json_data.get("message", None):
            print(
                "API RATE limit exceeded - Exiting, where do you think you are going ?"
            )
            sys.exit(1)
    except KeyError:
        pass
    return True


def record_query(
    searched_table: tinydb.table.Table, r_owner: str, r_name: str
) -> list:
    releases = open_query()
    return searched_table.search(
        (releases.repo_owner == r_owner) & (releases.repo_name == r_name)
    )


def open_query() -> tinydb.queries.Query:
    return tinydb.queries.Query()


def tag_comparison(
    query_table: tinydb.table.Table, data: dict, r_owner: str, r_name: str
) -> Union[bool, None]:
    search_results = record_query(query_table, r_owner, r_name)
    if search_results:
        TODAY = date.today()
        current_tag = search_results[0]["current_version"]
        return (
            update_tag(query_table, data, r_owner, r_name)
            if data.get("tag_name", None) > current_tag
            else print(
                f"{r_owner}/{r_name} - No new tags for today - {TODAY}\n"
            )
        )
    # if a record does not exist for this repo (first run)
    else:
        insert_data(query_table, data, r_owner, r_name)
    return False


def update_tag(
    table_to_update: tinydb.table.Table, data: dict, r_owner: str, repo: str
) -> bool:
    releases = open_query()
    print(f"New Tag for {repo}, updating the db.\n")
    table_to_update.update(
        {"current_version": data.get("tag_name", None)},
        releases.repo_owner == r_owner,
    )
    return True


def send_notification(
    alert_platform: str,
    data: dict,
    r_owner: str,
    r_name: str,
    py_test: bool = False,
) -> None:
    alert_platform = alert_platform.lower()
    if alert_platform and alert_platform == "slack":
        slack_alert(data, r_owner, r_name) if not py_test else None
        print("notification sent to Slack")


def insert_data(
    table: tinydb.table.Table, data: dict, r_owner: str, r_name: str
) -> None:
    print(
        f"No record exists in the DB for the {r_owner}/{r_name} subscription."
    )
    print("Building one now, you'll be ready for the next releases\n")
    table.insert(
        {
            "repo_owner": r_owner,
            "repo_name": r_name,
            "current_version": data.get("tag_name", None),
        }
    )


app = typer.Typer()


@app.command()
def main(
    yaml_config_path: str = typer.Argument(
        ...,
        envvar="YAML_CONFIG_FILE",
        help="The path to the configuration file.",
    ),
    tracking_db_file_path: Path = typer.Argument(
        ...,
        envvar="TRACKING_DB_FILE",
        help="The path where the db file exist, if the path does not exist, a db file will be created.",
    ),
    alert: bool = typer.Option(False, help="Enable or disable the alerting"),
    alert_platform: str = typer.Option(
        None,
        help="The platform used to notify new releases if --alert is set to true",
    ),
):
    YAML_CONFIG_PATH = check_if_yaml_exists(yaml_config_path)
    check_file_extension(YAML_CONFIG_PATH, "yaml")
    YAML_CONFIGURATION = validate_conf_file(YAML_CONFIG_PATH)
    check_alerts_args(alert, alert_platform)
    tiny_table = initialize_tracking_db(tracking_db_file_path)
    check_alert_platform(alert_platform) if alert else None
    for sub in YAML_CONFIGURATION["subscriptions"]:
        repo_owner, repo_name = sub["repo_owner"], sub["repo_name"]
        data = api_call(repo_owner, repo_name)
        upgraded_tag = tag_comparison(tiny_table, data, repo_owner, repo_name)
        send_notification(
            alert_platform, data, repo_owner, repo_name
        ) if alert and upgraded_tag else None


if __name__ == "__main__":
    app()
